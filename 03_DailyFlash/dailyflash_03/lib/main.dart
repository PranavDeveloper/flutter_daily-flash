import 'package:dailyflash_03/Quetion_01.dart';
import 'package:dailyflash_03/Quetion_02.dart';
import 'package:dailyflash_03/Quetion_03.dart';
import 'package:dailyflash_03/Quetion_01.dart';
import 'package:dailyflash_03/Quetion_05.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment1(),
    );
  }
}
