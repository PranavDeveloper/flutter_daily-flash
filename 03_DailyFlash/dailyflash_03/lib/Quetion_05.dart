import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget{
  const Assignment5({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
       title: const Text("Assignment 5", style:TextStyle(
        fontStyle: FontStyle.normal,
        fontSize: 30,
        fontWeight: FontWeight.w700
       )
       )
,
      ),
      body: Center(
        child:Container(
        height: 500,
        width: 500,
          
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 5),
            gradient:const LinearGradient(colors:[Colors.red,Colors.blue], stops: [0.5 ,0.5])
      
          ),
        )
      ),
    );
  }
}