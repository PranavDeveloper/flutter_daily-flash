import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});
  @override
   Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.amber,
        title: const Text("Assignment 2",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            border: Border(bottom: BorderSide(width: 5),left: BorderSide(width: 5),right: BorderSide(width: 5),top: BorderSide(width: 5)
            ),
            image: DecorationImage(image:NetworkImage("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Nelumno_nucifera_open_flower_-_botanic_garden_adelaide_edit3.jpg/800px-Nelumno_nucifera_open_flower_-_botanic_garden_adelaide_edit3.jpg?20080524204145"),
            fit: BoxFit.cover
            )
          ),
          child: const Center(
            child: Text("Pranav",
            style: TextStyle(fontSize: 30,
            fontWeight: FontWeight.w700),),
          ),
        ),
      ),
    );
   }
}