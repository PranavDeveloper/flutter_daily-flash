import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});
  @override 
  build(BuildContext context){
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: Colors.amber,
        
        centerTitle: true,
        title: const Text("Assignment 1",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
        
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            border: Border.symmetric(vertical:BorderSide(width: 5),horizontal: BorderSide(width: 5))
          ),
          padding: const EdgeInsets.all(9),
          child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTezeOQoE0czCijM3BdgimK6w9J6IRs31nJ24MzZpJapFupRvwUJyZxclDF6LcGuIwtpTs&usqp=CAU"),
        ),
      ),
    );
  }
}