import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget{
  const Assignment3({super.key});
  @override
  State createState()=> _Assignment3State();
}
class _Assignment3State extends State{
  Color borderColor =Colors.red;
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Assignment3",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700,
        ),
        ),
      ),
      body:
      Center(
        child: GestureDetector(
          onTap:() {
            setState(() {
              borderColor=Colors.green;
            });
          },
          child: Container(
            height: 500,
            width: 500,
            decoration: BoxDecoration(
              border: Border.all(width: 10,
              color: borderColor
              )
            ),
            child: const Center(
              child: Text("Tap Me",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w700
              ),),
            )
          ),
        ),
        
        
      )
      
    );
  }
}