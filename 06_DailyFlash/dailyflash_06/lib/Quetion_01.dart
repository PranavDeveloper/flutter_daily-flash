import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment 1",
        style: TextStyle(fontSize: 30,
        fontWeight: FontWeight.w700),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.network("https://img.freepik.com/free-photo/top-view-pepperoni-pizza-with-mushroom-sausages-bell-pepper-olive-corn-black-wooden_141793-2158.jpg"),
          const SizedBox(
            height: 20,
          ),
          const Padding(padding: EdgeInsets.all( 20),
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Text("Pizza",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w700
              ),
              ),
               SizedBox(
                height: 20,
              ),
              Text("A Large Circle of flat bread baked with cheese,tomatoesand vegetables spread on top",
              style:TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w300
              ))
            ],
          ) ,)
        ],
      ),
    );
  }
}