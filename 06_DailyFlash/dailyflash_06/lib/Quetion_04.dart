import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget{
  const Assignment4({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment 4",
        style: TextStyle(fontSize: 30,
        fontWeight: FontWeight.w700),
        ),
      ),
      body: Center (
        child: Padding(padding: EdgeInsets.all(20),
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 15,

        ),
        decoration: BoxDecoration(
          border: Border.all(),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                border: Border.all()
              ),
              height: 100,
              width:100 ,
              child:Container(
                height: 100,
                width: 100,
                color: Colors.red,
              ) ,
            ),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                border: Border.all()
              ),
              height: 100,
              width:100 ,
              child:Container(
                height: 100,
                width: 100,
                color: Colors.purple,
              ) ,
            )
          ],
        ),
      ),
      )
      )
    );
  }
}