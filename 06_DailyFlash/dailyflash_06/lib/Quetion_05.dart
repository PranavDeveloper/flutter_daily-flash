import 'package:dailyflash/Quetion_03.dart';
import 'package:flutter/material.dart';
class Assignment5 extends StatefulWidget{
  const Assignment5({super.key});
  @override
  State createState()=> _Assignment5State();

}
class _Assignment5State extends State{
  bool box1=false;
  bool box2=false;
  bool box3=false;
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment 5",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
      ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: (){
                box1=!box1;
                setState(() {
                  
                });
            
                
              },
              child: Container(
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: box1 ? Colors.white: Colors.red,
                  border: Border.all()
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                box2=!box2;
                setState(() {
                  
                });
            
                
              },
              child: Container(
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: box2 ? Colors.white: Colors.red,
                  border: Border.all()
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                box3=!box3;
                setState(() {
                  
                });
            
                
              },
              child: Container(
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: box3 ? Colors.white: Colors.red,
                  border: Border.all()
                ),
              ),
            )

          ],
        ),
      ) ,
    );
  }
}