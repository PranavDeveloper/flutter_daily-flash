
import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment 2",
        style: TextStyle(fontSize: 30,
        fontWeight: FontWeight.w700),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 250,
              width: 250,
              decoration:  BoxDecoration(
                 border: Border.all(
                  color:Colors.purple,
                  width: 5
                 ),
                 borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Image.network("https://img.freepik.com/free-photo/top-view-pepperoni-pizza-with-mushroom-sausages-bell-pepper-olive-corn-black-wooden_141793-2158.jpg",
              ),
            ),
             const SizedBox(
              height: 20,
             ),
             ElevatedButton(onPressed: (){},
              style: const ButtonStyle(
                fixedSize: MaterialStatePropertyAll(Size(250, 70)),
                backgroundColor: MaterialStatePropertyAll(Colors.purple)
              ), child: const Text("Add to Cart",
              style: TextStyle(color: Colors.black),
              ),
             )
             
          ],
        ),
      )
    );
  }
}