import 'package:flutter/material.dart';
class Assignment3 extends StatelessWidget{
  const Assignment3({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment 3",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 100,
              width: 100,
              decoration:  BoxDecoration(
                color: Colors.amber,
                borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight:Radius.circular(20)),
                border: Border.all(width: 3,color: Colors.black),
                boxShadow:const [BoxShadow(color: Colors.grey,offset: Offset(20, 30))]
                
              ),
            ),
            Container(
              height: 100,
              width: 100,
              decoration:  BoxDecoration(
                color: Colors.purple,
                borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight:Radius.circular(20)),
                border: Border.all(width: 3,color: Colors.black),
                boxShadow:const [BoxShadow(color: Colors.grey,offset: Offset(20, 30))]
                
              ),
            )
          ],
        ),
      )
    );
  }
}