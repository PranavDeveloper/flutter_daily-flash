import 'package:flutter/material.dart';
class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle:true,
        backgroundColor: Colors.amber,
        title: const Text("Assignment1",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 100,
            width: 100,
            color: Colors.purple,
          ),
          Container(
            height: 80,
            width: 80,
            color: Colors.pink,
          ),
          Container(
            height: 70,
            width: 80,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}