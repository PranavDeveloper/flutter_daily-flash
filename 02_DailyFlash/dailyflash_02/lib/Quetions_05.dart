import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment5 extends StatefulWidget{
  const Assignment5({super.key});
  @override
  State createState()=>_Assignment5State();
}
class _Assignment5State extends State{
  bool isButtonPressed=false;
  void _handleButtonPresse(){
    setState(() {
      isButtonPressed =! isButtonPressed;
    });
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:  const Text("Assignment 5", style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            _handleButtonPresse();
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: isButtonPressed ? Colors.red : Colors.blue,
          ),
          child: Container(
            width: 200,
            height: 100,
            alignment: Alignment.center,
            child: Text(
              isButtonPressed ? 'click me' : 'container tapped',
              style:const  TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),

    );
  }
}