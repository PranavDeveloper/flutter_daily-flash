import 'package:flutter/material.dart';


class Assignment4 extends StatelessWidget {
  const Assignment4({super.key});
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Assignment 4', style: TextStyle(fontSize: 30,fontWeight: FontWeight.w700),),
          backgroundColor: Colors.amber,
        ),
        body: Center(
          child: Container(
            width: 300,
            height: 100,
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 244, 226, 232),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
              border: Border.all(
                color: Colors.blue, 
                width: 5,
              ),
            ),
             child: const Text("pranav"),
          ),
        ),
      );
  }
}