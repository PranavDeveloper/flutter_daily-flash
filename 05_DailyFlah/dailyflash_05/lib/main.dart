import 'package:dailyflash_05/Quetion1.dart';
import 'package:dailyflash_05/Quetion2.dart';
import 'package:dailyflash_05/Quetion3.dart';
import 'package:dailyflash_05/Quetion4.dart';
import 'package:dailyflash_05/Quetion5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment5(),
    );
  }
}
