import 'package:flutter/material.dart';
class Assignment4 extends StatelessWidget{
  const Assignment4({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      
      
      appBar:  AppBar(
        backgroundColor: Colors.amber,
        centerTitle: true,
        title: const Text("Assignment 4",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
      ),
      body: Center(
        child:  Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.blue,
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.orange,
            ),
          ],
        ),
      ),
    );
  }
}