import 'package:flutter/material.dart';
class Assignment3 extends StatelessWidget{
  const Assignment3({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      
      
      appBar:  AppBar(
        backgroundColor: Colors.amber,
        centerTitle: true,
        title: const Text("Assignment 3",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
      ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 200,
              width: 300,
              child: Image.asset("assets/images/Pranav.jpeg"),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 100,
              width: 100,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(width: 3),
                borderRadius:const  BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20),
                ),
                boxShadow:const [BoxShadow(
                  color: Colors.grey,
                  blurRadius: 5.0,
                  offset: Offset(0, 2)
                )
                ]
              ),
              child:const Center(
                child: Text("Pranav",
                style:TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700
                ) ,)
              ),
            )
          ],
        ),
      ) ,
    );
  }
}