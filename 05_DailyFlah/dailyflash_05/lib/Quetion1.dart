import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        centerTitle: true,
        title: const Text("Personal Information",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
        backgroundColor: Colors.amber,
      ),
      body:  Column(
        
        mainAxisAlignment: MainAxisAlignment.start ,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 40,
          ),
          Container(
            width: 250,
            height: 250,
            child: Image.asset("assets/images/Pranav.jpeg"),
          ),
          const SizedBox(
            height: 20,
          ),
           Container(
            padding: const EdgeInsets.only(left: 70),
        
            child:  const Text("Pranav",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w700
            ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.only(left: 50),
            child: const Text("7218563877",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w700
            ),),
          )
          
        ],
      ),
    );
  }
}