import 'package:flutter/material.dart';
class Assignment5 extends StatelessWidget{
  const Assignment5({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      
      
      appBar:  AppBar(
        backgroundColor: Colors.amber,
        centerTitle: true,
        title: const Text("Assignment 5",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Image.network("https://img.freepik.com/free-photo/natures-beauty-captured-colorful-flower-close-up-generative-ai_188544-8593.jpg?size=626&ext=jpg&ga=GA1.1.1827530304.1709769600&semt=ais"),
            const Spacer(),
            Container(
              height: 50,
              width: 50,
              color: Colors.red,
            ),
            Container(
              height: 50,
              width: 50,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}