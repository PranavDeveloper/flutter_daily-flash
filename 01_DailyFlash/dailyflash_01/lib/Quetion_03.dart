
import 'package:flutter/material.dart';
class Assignment3 extends StatelessWidget{
  const Assignment3({super.key,});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Assignment3", style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(20), bottomLeft: Radius.circular(20)),
        ),
        backgroundColor: Colors.pink,
        
      ),
    );
  }

}