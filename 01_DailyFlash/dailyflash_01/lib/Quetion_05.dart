
import 'package:flutter/material.dart';
class Assignment5 extends StatelessWidget{
  const Assignment5({super.key,});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Assignment 4", style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
        backgroundColor: Colors.pink,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(color: Colors.red,offset: Offset(20, 20),blurRadius: 8)
                ],

                color:Colors.yellow,
                border:Border.all(color: Colors.pink , width: 1)

                ),
              
              )
            ],
          ),
        ),
        
      );
    
  }

}