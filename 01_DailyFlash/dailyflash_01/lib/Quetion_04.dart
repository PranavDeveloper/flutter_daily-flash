
import 'package:flutter/material.dart';
class Assignment4 extends StatelessWidget{
  const Assignment4({super.key,});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Assignment 4", style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
        backgroundColor: Colors.pink,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(borderRadius:BorderRadius.zero,
                color:Colors.blue,
                border:Border.all(color: Colors.red , width: 10)

                ),
              
              )
            ],
          ),
        ),
        
      );
    
  }

}