import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.home , size: 40), 
        backgroundColor: Colors.red,
        actions: const [
          Icon(Icons.search,size: 40,),
           SizedBox(width: 50),
          Icon(Icons.menu,size: 40),
           SizedBox(width: 50),
          Icon(Icons.label, size:40)
        ],
      ),
    );
  }
}
