

import 'package:flutter/material.dart';
class Assignment1 extends StatelessWidget{
  const Assignment1({super.key,});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.menu, size: 40,),
        centerTitle: true,
        title: const Text("Assignment1", style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),
        ),
        actions: const [
          Icon(Icons.search ,size:40)
        ],
        backgroundColor: Colors.amber,
        
      ),
    );
  }

}