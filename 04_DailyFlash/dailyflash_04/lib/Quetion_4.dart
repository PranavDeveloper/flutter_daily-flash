import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget{
  const Assignment4({super.key});
  @override
  State createState()=>_Assignment4State();
  }
  class _Assignment4State extends State{
    @override
    Widget build(BuildContext context){
      return Scaffold(
        appBar: AppBar(
          title: const Text("floating Action Button"),
        ),
          floatingActionButton: FloatingActionButton(
        onPressed: () {},
        hoverColor: Colors.orange,
      ),
      
      );
    }
  }