import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Assignment2", style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w700
        ),),
      ),
      body: Center(
        child: ElevatedButton(onPressed: (){},
        style: ElevatedButton.styleFrom(
          shadowColor: Colors.amber,
          shape: const CircleBorder(),
          minimumSize: const Size(200, 200),
          side: const BorderSide(color:Colors.red,width: 4)
        ),
        child: const Text("click me"),),
      ),
    );
  }
}