import 'package:dailyflash_04/Quetion_1.dart';
import 'package:dailyflash_04/Quetion_2.dart';
import 'package:dailyflash_04/Quetion_3.dart';
import 'package:dailyflash_04/Quetion_4.dart';
import 'package:dailyflash_04/Quetion_5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home:Assignment5(),
    );
  }
}
