import 'package:flutter/material.dart';
class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment1 ",
        style: TextStyle(fontSize: 30,
        fontWeight: FontWeight.w700),),
        centerTitle: true,

      ),
      body: Center(
        child:ElevatedButton(onPressed: (){},
        style: ElevatedButton.styleFrom(
          shadowColor: Colors.red,
          
          elevation: 8,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))
        ), child: const Text("Click here"),
        )
      ),
    );
  }
}